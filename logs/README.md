This file is primarily here so that the structure of the data directory is carried with the repository.
The files will look as follows:
```
NEOs
|
|\
| +- code
| | \
| | +- various scripts and notebooks
| |
| |
| +- logs
| | \
| | +- <optimizer>_c<n_conv>_f<n_fc>_r<learning rate>_k<kernel size>
| | | \
| | | +- events.out.tfevents...
| | | 
| | +- <optimizer>_c<n_conv><f<n_fc>_r<learning rate>_k<kernel size>
| | | \
| | | +- events.out.tfevents...
: : :
```
