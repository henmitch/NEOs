This file is primarily here so that the structure of the data directory is carried with the repository.
The files will look as follows:
```
NEOs
|
|\
| +- code
| | \
| | +- various scripts and notebooks
| |
| |
| +- data
| | \
| | +- fits
| | | \
| | | +- scan_id 1
| | | | \
| | | | +- 001
| | | | | \
| | | | | +- w1.fits
| | | | | +- w2.fits
| | | | +- 002
| | | | | \
| | | | | +- w1.fits
| | | | | +- w2.fits
| | | | :
| | | | :
| | | | +- 275
| | | | | \
| | | | | +- w1.fits
| | | | | +- w2.fits
| | | |
| | | +- scan_id 2
| | | | \
| | | | +- 001
| | | | | \
| | | | | +- w1.fits
| | | | | +- w2.fits
| | | | +- 002
| | | | | \
| | | | | +- w1.fits
| | | | | +- w2.fits
| | | | :
| | | | :
| | | | +- 275
| | | | | \
| | | | | +- w1.fits
| | | | | +- w2.fits
| | | :
: : : :
```
