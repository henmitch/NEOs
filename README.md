# NEOs
The goal of this project is to explore and identify machine-learning use cases for studying near-earth objects (NEOs) in satellite image data.

Please see [the documentation](https://henmitch.gitlab.io/NEOs) for, well, the documentation.

## Versions
The following software was used in developing this project:
+ Python 3.6.2
+ `tensorflow` - v1.7.0
+ `numpy` - v1.13.3
+ `pandas` - v0.23.1
+ `requests` - v2.19.1
+ `astropy` - v3.0.3

Of course, that's all visible from the `environment.yml` file that Anaconda (`conda` - v3.19.0) generated for me, but still.
Most of those packages came from installing those 5 packages above.

This software was initially developed on a laptop running Windows 10, but has now moved over to [GSFC](https://gsfc.nasa.gov)'s Minerva cluster, which is running RHEL 6.9 (Santiago).

## Acknowledgements
This internship was sponsored by Vermont Space Grant Consortium under NASA Grant and Cooperative Agreement NNX15AP86H.

This project makes use of data products from the Near-Earth Object Wide-field Infrared Survey Explorer (NEOWISE), which is a project of the Jet Propulsion Laboratory (JPL)/California Institute of Technology (Caltech).
NEOWISE is funded by the National Aeronautics and Space Administration.

This research made use of Montage. It is funded by the National Science Foundation under Grant Number ACI-1440620, and was previously funded by the National Aeronautics and Space Administrationís Earth Science Technology Office, Computation Technologies Project, under Cooperative Agreement Number NCC5-626 between NASA and the California Institute of Technology.
