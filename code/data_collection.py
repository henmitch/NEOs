import logging
import numpy as np
import pandas as pd
import pickle
import os
import requests
import sys

from astropy.io import fits
from io import BytesIO
from typing import List, Tuple, Union

# The default location to put collected image data
FITS_DIR = os.path.join("..", "data", "fits")
# The base of the URL for pulling in image data
FITS_BASE = "http://irsa.ipac.caltech.edu/ibe/data/wise/neowiser/p1bm_frm"

# The base of the URL for pulling in counts
TAP_BASE = "https://irsa.ipac.caltech.edu/TAP/sync?QUERY={:s}&FORMAT=CSV"
# The bones of the query for pulling in counts
TAP_QRY = "SELECT+frame_num,cntr+FROM+neowiser_p1bs_psd+WHERE+(scan_id='{}'{})"

# Logging
logger = logging.getLogger()
logger.setLevel(logging.WARN)
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.debug("Logger is working")


def get_image_data(
    scan_id: str,
    frame_num: Union[int, str] = "all",
    band: int = 1,
    output_file: str = None,
    include_headers: bool = True,
    scale_data=False
) -> Union[Tuple[fits.Header, np.ndarray],
           Tuple[List[fits.Header], np.ndarray]]:
    """
    Fetch a header and data array from IRSA
    scan_id:     (String) The id of the scan in question
    frame_num:   (Integer) The frame number of the image in question
    band:        (Integer) Which IR band of WISE's detectors to pull from.
    output_file: (String) Where to put the FITS data.
                    Defaults to `../data/fits/{coadd_id}/w{band}.fits`
    include_headers: (Boolean) Whether to include the FITS header
    scale_data:

    return header: (astropy.fits.Header) The FITS header from the first card.
    return data:   (numpy.ndarray) The data array associated with the FITS file
    """
    scan_dir = os.path.join(
        FITS_DIR,
        scan_id
    )
    scaled = "_scaled" if scale_data else ""
    # Default storage location for the image data
    if frame_num == "all":
        logger.info(
            "Collecting all frames for scan_id: {:s}".format(scan_id)
        )
        check = os.path.join(
            scan_dir,
            "{:s}_w{:d}_frames{}.pkl".format(scan_id, band, scaled)
        )
    else:
        logger.info(
            "Collecting scan_id: {:s} frame: {:03d}".format(scan_id, frame_num)
        )
        check = os.path.join(
            scan_dir,
            "{:03d}".format(frame_num),
            "w{}{}.fits".format(band, scaled)
        )
    logger.debug(f"Path: {check}")

    if output_file:
        filename = output_file
    else:
        filename = check

    if scale_data:
        scale_fn = lambda x: scale(x, log=True)
    else:
        scale_fn = lambda x: x

    if frame_num == "all":
        # If you can't load them locally...
        if not os.path.exists(check):
            headers = []
            images = []
            # ... download them...
            for i in range(1, 276):
                header, image = get_image_data(scan_id, int(i), band, scale_data=scale_data)
                if image.size > 5:
                    headers.append(header)
                    images.append(scale_fn(remove_nans(image)))
            images = np.array(images)

            dirname = os.path.dirname(filename)
            if not dirname == "" and not os.path.exists(dirname):
                logger.info("Creating path: " + os.path.dirname(filename))
                os.makedirs(os.path.dirname(filename))
            # ... and save them
            with open(filename, "wb") as f:
                pickle.dump([headers, images], f)

        # If you can load them locally, do so
        else:
            logger.info(
                "Found scan_id: {:s} w{} locally".format(scan_id, band)
            )
            with open(check, "rb") as f:
                headers, images = pickle.load(f)

        return (headers, images) if include_headers else images

    # If you can't load it locally...
    if not os.path.exists(check):
        url = "{:s}/{:s}/{:s}/{:03d}/{:s}{:03d}-w{:1d}-int-1b.fits".format(
            FITS_BASE, scan_id[-2:], scan_id,
            frame_num, scan_id, frame_num, band
        )
        logger.debug(url)
        # ... download it...
        response = requests.get(url)
        if response.status_code == 200:
            logger.info(
                "Download scan_id: {:s} frame: {:03d} w{} successfully".format(
                    scan_id, frame_num, band
                ))
        else:
            logger.warning(
                "Err scan_id: {:s} frame: {:03d} w{}; return status {}".format(
                    scan_id, frame_num, band, response.status_code
                ))
            # TODO: figure out how to deal with these
            header = fits.Header()
            image = np.empty([1])
            return (header, image) if include_headers else image
        fits_file = response.content

        # ... and save it
        dirname = os.path.dirname(filename)
        if not dirname == "" and not os.path.exists(dirname):
            logger.info("Creating path: " + os.path.dirname(filename))
            os.makedirs(os.path.dirname(filename))
        with open(filename, "wb") as f:
            f.write(fits_file)
            logger.info("Wrote to {}".format(filename))
        with fits.open(BytesIO(fits_file)) as a:
            header = a[0].header
            data = a[0].data
    # If you can load it locally, do so
    else:
        logger.info("Found scan_id: {:s} frame: {:03d} w{} locally".format(
            scan_id, frame_num, band
        ))
        with open(check, "rb") as f:
            with fits.open(f) as a:
                header = a[0].header
                data = a[0].data

    return (header, scale_fn(remove_nans(data))) if include_headers else remove_nans(data)


def get_object_count(scan_id: str, frame_num: int = None) -> List[int]:
    """
    Count the number of objects detected in the scan (and frame, if desired)
    scan_id:     (String) The id of the scan in question
    frame_num:   (Integer) The frame number of the image in question

    return num_objects: (List) A list of numbers of objects in the frames
    """
    # Default location for the count data
    check = os.path.join(FITS_DIR, scan_id, "{:s}_counts.pkl".format(scan_id))
    # If you can't find it locally...
    if not os.path.exists(check) or frame_num:
        # If someone asks for a specific frame number (which would be dumb),
        # make it part of the query.
        if frame_num:
            query_arg = "+and+frame_num={}".format(frame_num)
            logger.info(
                "Count scan_id: {} frame: {:03d}".format(scan_id, frame_num)
            )
        else:
            query_arg = ""
            logger.info("Count scan_id: {}".format(scan_id, frame_num))
        url = TAP_BASE.format(TAP_QRY.format(scan_id, query_arg))
        logger.debug(url)

        # Download all the objects' IDs in each frame
        cntrs = requests.get(url)
        if cntrs.status_code == 200:
            csv = BytesIO(cntrs.content)
            # Count the number of objects in each frame
            grouped = pd.read_csv(csv).groupby("frame_num")
            num_objects = list(grouped.count()["cntr"])
            dirname = os.path.dirname(check)
            dirname = os.path.dirname(check)

            if not dirname == "" and not os.path.exists(dirname):
                logger.info("Creating path: " + os.path.dirname(check))
                os.makedirs(os.path.dirname(check))
            with open(check, "wb") as f:
                pickle.dump(num_objects, f)
                logger.info("Wrote to {}".format(check))
            return num_objects

        else:
            # TODO: same as above.  Figure out how to deal with these.
            logging.warning("Bad query: {}".format(cntrs.status_code))
            return [0]

    else:
        # Found it locally!
        logger.info("Count scan_id: {} locally".format(scan_id))
        with open(check, "rb") as f:
            num_objects = pickle.load(f)
        return num_objects


def remove_nans(data: np.ndarray) -> np.ndarray:
    """
    Recursively remove all nans from a 2-dimensional numpy array
    param data:  (numpy.ndarray) The data to remove the nans from

    return data: (numpy.ndarray) The data with the nans removed
    """
    # Set up our direction vectors
    up = np.array([-1, 0])
    right = np.array([0, 1])
    # and our direction matrix
    directions = np.array([
        right,
        up,
        -right,
        -up
    ])
    edge = np.array([1, 1, 1, 1]).reshape([-1, 1])
    # Check everything that isn't the edges first
    # TODO: streamline this
    nan_indices = np.argwhere(np.isnan(data[1:-1, 1:-1])) + np.array([1, 1])
    ups = nan_indices + up
    rights = nan_indices + right
    downs = nan_indices - up
    lefts = nan_indices - right

    center_surrounds = np.array([data[place.T.tolist()] for place in [
        ups,
        rights,
        downs,
        lefts
    ]])

    data[nan_indices.T.tolist()] = np.nanmean(center_surrounds, axis=0)

    # Check the corners next
    if np.isnan(data[-1, 0]):
        data[-1, 0] = np.nanmean(np.array([
            data[-2, 0], data[-1, 1], data[-2, 1]
        ]))
    if np.isnan(data[0, -1]):
        data[0, -1] = np.nanmean(np.array([
            data[0, -2], data[1, -1], data[0, -2]
        ]))
    if np.isnan(data[-1, -1]):
        data[-1, -1] = np.nanmean(np.array([
            data[-1, -2], data[-2, -2], data[-2, -1]
        ]))
    if np.isnan(data[0, 0]):
        data[0, 0] = np.nanmean(np.array([
            data[1, 0], data[1, 1], data[0, 1]
        ]))

    for i, side in enumerate(["right", "top", "left", "bottom"]):
        edge = np.array([1, 1, 1, 1]).reshape([-1, 1])
        if side == "right":
            nans = np.argwhere(np.isnan(data[1:-1, -1])) + 1
            nans = np.concatenate(
                (
                    nans,
                    (data.shape[1] - 1)*np.ones_like(nans)
                ),
                axis=1
            )
        elif side == "top":
            nans = np.argwhere(np.isnan(data[0, 1:-1])) + 1
            nans = np.concatenate((np.zeros_like(nans), nans), axis=1)
        elif side == "left":
            nans = np.argwhere(np.isnan(data[1:-1, 0])) + 1
            nans = np.concatenate((nans, np.zeros_like(nans)), axis=1)
        else:
            nans = np.argwhere(np.isnan(data[-1, 1:-1])) + 1
            nans = np.concatenate(
                (
                    (data.shape[1] - 1)*np.ones_like(nans),
                    nans),
                axis=1
            )

        if nans.size == 0:
            continue
        edge[i] = 0
        a = np.array(4*[nans])
        surround = np.array([edge*directions]*a.shape[1]).reshape(*a.shape) + a
        data[nans.T.tolist()] = np.nanmean(data[surround.T.tolist()], axis=1)

    if np.argwhere(np.isnan(data)).size == 0:
        return data
    else:
        return remove_nans(data)


def scale(data: np.ndarray, log: bool = False) -> np.ndarray:
    """
    Normalize the data so it is in the range [-1, 1]
    param data:  (numpy.ndarray) What data to scale
    param log:   (Boolean) Whether or not to take the logarithm of data

    return data: (numpy.ndarray) The scaled data
    """
    # Shift the data so that its minimum is 1
    data -= data.min() - 1
    # Log the data, if necessary
    if log:
        data = np.log10(data)
    # Shift the data so that its minimum is 0
    data -= data.min()
    # Scale the data so that its maximum is 2
    data = 2*data/data.max()
    # Shift the data so that its minimum is -1, maximum is 1
    data -= 1

    return data
