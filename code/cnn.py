import data_collection as dc
import numpy as np
import os
import tensorflow as tf

from itertools import product
from multiprocessing import Pool

dc.logger.setLevel(dc.logging.INFO)

tf.logging.set_verbosity(tf.logging.INFO)

SCAN_IDS = os.listdir("../data/fits")
BATCH_SIZE = 10

print(SCAN_IDS)

def conv_layer(input_layer, depth_in, depth_out, kernel_size=[5, 5], strides=[1, 1, 1, 1], name="conv"):
    with tf.name_scope(name):
        w = tf.Variable(tf.truncated_normal([*kernel_size, depth_in, depth_out], stddev=0.1), name="W")
        b = tf.Variable(tf.constant(0.25, shape=[depth_out]), name="B")
        conv = tf.nn.conv2d(input_layer, w, strides=strides, padding="SAME")
        act = tf.nn.relu(conv + b)
        tf.summary.histogram("weights", w)
        tf.summary.histogram("biases", b)
        tf.summary.histogram("activations", act)
        return tf.nn.max_pool(act, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME")

def fc_layer(input_layer, size_in, size_out, name="fc"):
    with tf.name_scope(name):
        w = tf.Variable(tf.truncated_normal([size_in, size_out], stddev=0.1), name="W")
        b = tf.Variable(tf.constant(0.25, shape=[size_out]), name="B")
        act = tf.matmul(input_layer, w) + b
        tf.summary.histogram("weights", w)
        tf.summary.histogram("biases", b)
        tf.summary.histogram("activations", act)
        return act

def arch(x, n_conv, n_fc, k):
    conv1 = conv_layer(
        x,
        depth_in=1,
        depth_out=40,
        kernel_size=[k, k],
        name="conv1"
    )

    conv2 = conv_layer(
        conv1,
        depth_in=40, depth_out=20,
        kernel_size=[k, k],
        name="conv2"
    )

    if n_conv == 2:
        keep = conv2
        size = 254*254*20
    elif n_conv == 3:
        keep = conv_layer(
            conv2,
            depth_in=20, depth_out=10,
            kernel_size=[k, k],
            name="conv3"
        )
        size = 127*127*10
    elif n_conv == 4:
        keep = conv_layer(
            conv3,
            depth_in=10, depth_out=5,
            kernel_size=[k, k],
            name="conv4"
        )
        size = 64*64*5
    else:
        raise ValueError("Invalid number of convolutional layers")

    flat = tf.reshape(keep, [-1, size])

    fc1 = fc_layer(
        flat,
        size_in=size, size_out=508,
        name="fc1"
    )

    if n_fc == 2:
        final = fc_layer(
            fc1,
            size_in=508, size_out=1,
            name="prediction"
        )
    elif n_fc == 3:
        fc2 = fc_layer(
            fc1,
            size_in=508, size_out=254,
            name="fc2"
        )
        final = fc_layer(
            fc2,
            size_in=254, size_out=1,
            name="prediction"
        )
    else:
        raise ValueError("Invalid number of fully-connected layers")

    return final

learning_rates = [1e-6, 1e-5, 1e-4, 2e-4]
n_convs = [2, 3, 4]
n_fcs = [2, 3]
ks = [3, 5, 7]
optimizer_names = ["adam", "adagrad", "adadelta", "momentum"]
momenta = [0.8, 0.85, 0.9]

params = [(
    n_conv, n_fc, k,
    optimizer_name, learning_rate,
    momentum
) for (
    n_conv, n_fc, k,
    optimizer_name, learning_rate,
    momentum
) in product(
    n_convs, n_fcs, ks,
    optimizer_names, learning_rates,
    momenta
)]

counts = []
for scan_id in SCAN_IDS:
    if scan_id == SCAN_IDS[0]:
        images = dc.get_image_data(scan_id, include_headers=False, scale_data=True)
        counts = dc.get_object_count(scan_id)
    else:
        image = dc.get_image_data(scan_id, include_headers=False, scale_data=True)
        count = dc.get_object_count(scan_id)
        if len(count) != image.shape[0]:
            print(f"scan_id {scan_id} had image count {image.shape[0]} and object count {len(count)}")
            continue
        images = np.vstack((images, image))
        counts += count
counts = np.array(counts)/1e4

print(images.max(), images.min(), images.shape)
print(counts.max(), counts.min(), counts.shape)

def run(n_conv, n_fc, k, optimizer_name, learning_rate, momentum):
    tf.reset_default_graph()
    session_conf = tf.ConfigProto(
              intra_op_parallelism_threads=1,
              inter_op_parallelism_threads=1
    )
    sess = tf.Session(config=session_conf)
    optimizers = {
        "momentum": tf.train.MomentumOptimizer(learning_rate=learning_rate, momentum=momentum),
        "adam": tf.train.AdamOptimizer(learning_rate=learning_rate),
        "adagrad": tf.train.AdagradOptimizer(learning_rate=learning_rate),
        "adadelta": tf.train.AdadeltaOptimizer(learning_rate=learning_rate)
    }
    optimizer = optimizers[optimizer_name]

    logdirs = {
        "momentum": f"momentum_c{n_conv}_f{n_fc}_r{learning_rate}_k{k}_m{momentum}",
        "adam": f"adam_c{n_conv}_f{n_fc}_r{learning_rate}_k{k}",
        "adagrad": f"adagrad_c{n_conv}_f{n_fc}_r{learning_rate}_k{k}",
        "adadelta": f"adadelta_c{n_conv}_f{n_fc}_r{learning_rate}_k{k}"
    }
    logdir = os.path.join(os.curdir, "..", "logs", logdirs[optimizer_name])
    if os.path.exists(logdir):
        return

    images_tensor = tf.placeholder(tf.float32)
    counts_tensor = tf.placeholder(tf.float32)

    data = tf.data.Dataset.from_tensor_slices((images_tensor, counts_tensor))
    data = data.shuffle(buffer_size=1000000000).batch(BATCH_SIZE)
    iterator = data.make_initializable_iterator()

    t = iterator.get_next()
    x = tf.reshape(t[0], [-1, 1016, 1016, 1])
    y = tf.reshape(t[1], [-1, 1])
    tf.set_random_seed(25)

    final = arch(x, n_conv=n_conv, n_fc=n_fc, k=k)
    loss = tf.losses.mean_squared_error(y, final)
    with tf.name_scope("train"):
        train = optimizer.minimize(loss)

    tf.summary.scalar("loss", loss)
    tf.summary.scalar("relative", tf.reduce_mean((final - y)/y))

    sess.run(tf.global_variables_initializer())
    sess.run(iterator.initializer, feed_dict={images_tensor: images, counts_tensor: counts})

    writer = tf.summary.FileWriter(logdir, sess.graph)
    summary = tf.summary.merge_all()

    i = 0
    print(logdir)
    saver = tf.train.Saver()
    while True:
        try:
            _, loss_value, summarized = sess.run((train, loss, summary))
            if i % 10 == 0:
                print(logdirs[optimizer_name], i, loss_value, np.sqrt(loss_value))
                saver.save(sess, os.path.join(logdir, "model.ckpt"))
            writer.add_summary(summarized, global_step=i)
            i += 1
        except tf.errors.OutOfRangeError:
            break
    writer.close()
    sess.close()

with Pool(3) as p:
    p.starmap(run, params)

