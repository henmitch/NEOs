\documentclass{beamer}

\usepackage[arrowdel]{physics}
\usepackage{siunitx}
\usepackage[en-US]{datetime2}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{animate}

\author{Henry Mitchell}
\title{Machine Learning and its Application to NEOs}
\DTMsavedate{date}{2018-06-22}
\date{\DTMusedate{date}}

\definecolor{links}{HTML}{2A1B81}
\hypersetup{colorlinks,linkcolor=links,urlcolor=links}
\usefonttheme[onlymath]{serif}

\AtBeginSubsection[]{\begin{frame}
    \frametitle{Progress}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

\AtBeginSubsubsection[]{\begin{frame}
    \frametitle{Progress}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

\begin{document}
\frame{\titlepage}

\section{Machine Learning}
\begin{frame}
  \frametitle{Roadmap}
  \tableofcontents
\end{frame}

\subsection{An Overview}

\begin{frame}
  \frametitle{An Overview}
  Machine learning is a way to develop models that improve themselves as they take in more data.
  Particularly, the computer guesses a model, compares its predictions to reality, calculates how wrong the model was, and updates the model accordingly.

\end{frame}

\begin{frame}
  \frametitle{Types of Machine Learning}
  \begin{itemize}
    \item<1-> Paradigms
      \begin{itemize}
        \item<1-> Supervised
          \begin{itemize}
            \item<1-> Provide \textbf{features} (inputs) and \textbf{labels} (outputs) for training
            \item<1-> Most commonly used, most likely to provide actionable results
          \end{itemize}

        \item<1-> Unsupervised
          \begin{itemize}
            \item<1-> Only provide features, don't have to know labels
            \item<1-> Experimental, currently mostly academic
          \end{itemize}

      \end{itemize}

    \item<2-> Estimators
      \begin{itemize}
        \item<2-> Linear
          \begin{itemize}
            \item<2-> Simplest architecture (no hidden layers)
            \item<2-> Only discovers linear relationships
          \end{itemize}

        \item<2-> Neural Net/Deep Learning
          \begin{itemize}
            \item<2-> Hidden layers increase power, but also computational intensity
            \item<2-> Can find nonlinear relationships
          \end{itemize}

      \end{itemize}

    \item<3-> Goals
      \begin{itemize}
        \item<3-> Regression
          \begin{itemize}
            \item<3-> Return a real number
          \end{itemize}

        \item<3-> Classification
          \begin{itemize}
            \item<3-> Return one in a list of classes
          \end{itemize}
      \end{itemize}

  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{For the Purposes of This Talk\ldots}
  We will be dealing only with supervised learning.
  It's what's most practical for most situations.

  We will also be moving back and forth between regressors and classifiers.
  This is valid because:
  \begin{enumerate}
    \item The concepts and architectures are the same.
    \item Classifiers are essentially regressors with multiple outputs.

      For example, if we were trying to guess what a hand-written digit was, the label for a ``$5$'' would be $\bqty{0, 0, 0, 0, 0, 1, 0, 0, 0, 0}$.
      The computer would (hopefully) guess something along the lines of $\bqty{0.01, 0.1, 0.1, 0.05, 0.2, 0.98, 0.3, 0.1, 0.25, 0.02}$.
  \end{enumerate}

\end{frame}

\subsection{Linear Models}
\begin{frame}
  \frametitle{Conventions}
  I will use the following conventions in this talk:
  \begin{itemize}
    \item<1-> $\va{x} = \bqty{x_{1}, \ldots, x_{n}} \in \mathbb{R}^{n}$ is the input (features) to the model

    \item<2-> $\vu{y} = \bqty{\hat{y}_{1}, \ldots, \hat{y}_{m}} \in \mathbb{R}^{m}$ is the \textit{predicted} output (labels) from the model

    \item<3-> $\va{y} = \bqty{y_{1}, \ldots, y_{m}} \in \mathbb{R}^{m}$ is the \textit{actual} labels for the model

    \item<4-> $W = \sbmqty{W_{11} & \cdots & W_{1n} \\ \vdots & \ddots & \vdots \\ W_{m1} & \cdots & W_{mn}} \in \mathbb{R}^{m \times n}$ will be the weights of a linear model (don't worry, we'll get there in just a moment)

    \item<5-> $\va{b} = \bqty{b_{1}, \ldots, b_{m}} \in \mathbb{R}^{m}$ is the biases of the model

    \item<6-> $L(W, \va{b}, \vu{y}, \va{y}) \in \mathbb{R}$ is a generic loss function

    \item<7-> $\grad_{\scriptscriptstyle{W}}{L} = \sbmqty{\pdv{L}{W_{11}} & \cdots & \pdv{L}{W_{1n}} \\ \vdots & \ddots & \vdots \\ \pdv{L}{W_{m1}} & \cdots & \pdv{L}{W_{mn}}} \in \mathbb{R}^{m \times n}$ is the gradient of $L$ with respect to $W$

  \end{itemize}

\end{frame}

\subsubsection{Abstractly}
\begin{frame}
  \frametitle{Linear Models}
  They're exactly what they sound like.
  We decide on a \textbf{loss function} $L$ which will tell us (and the computer) how bad the model is.
  Common loss functions are mean squared error, accuracy, cross-entropy, etc.

  The computer develops a model of the form
  \[
    \vu{y} = W \va{x} + \va{b}
  \]
  by varying $W$ and $\va{b}$ to minimize $L$.

\end{frame}

\begin{frame}
  \frametitle{Dense Layers}
  Dense (or fully connected) layers are ``the kitchen sink'' of machine learning.
  With $n$ inputs and $m$ outputs, they have $n \times m$ connections, so $n \times m$ weights to learn.
  
\end{frame}

\begin{frame}
  \frametitle{Linear Model Optimization}
  We find be the best values for $W$ and $\va{b}$ by minimizing $L$.
  There are many algorithms for finding $\min(L)$, the simplest of which is gradient descent.
  \begin{enumerate}
    \item Find $\vu{y} = W \va{x} + \va{b}$

    \item Find $L(W, \va{b}, \vu{y}, \va{y})$

    \item Find $\grad_{\scriptscriptstyle{W}} L$

    \item Update $W$ as $W - \rho \grad_{\scriptscriptstyle{W}}{L}$.

    \item Repeat until satisfied

  \end{enumerate}
  The \textbf{hyperparameter} $\rho$ is the \textbf{learning rate} of the model.
  It's a constant that determines how much each gradient step should matter.

\end{frame}

\subsubsection{Concretely}
\begin{frame}
  \frametitle{The Setup (Example 1)}
  We'll take an extremely simple (1-dimensional) example to demonstrate the concept of gradient descent, and some of the pitfalls associated with it.

  Say we train our model, keeping track of the weights over time.
  We end up with the convergence shown on the next slide

\end{frame}

\begin{frame}
  \frametitle{Demonstration (Example 1)}
  \animategraphics[width=\linewidth]{5}{figure/local_min/anim_}{00}{29}

\end{frame}

\begin{frame}
  \frametitle{The Full Picture (Example 1)}
  This is a good thing, right?

  It is, unless our loss actually looks as follows:
  \begin{center}
    \includegraphics[width=\textwidth]{figure/local_min/local_min}
  \end{center}

\end{frame}

\begin{frame}
  \frametitle{The Pitfall}
  We found a \textit{local} minimum, not an absolute minimum.
  This is something that happens a lot, especially in higher-dimensional systems.

  We can deal with it in a few ways:
  \begin{enumerate}
    \item Try a lot of different initial conditions
    \item Accept that this is an issue with machine learning and take a suboptimal model
  \end{enumerate}

\end{frame}

\begin{frame}
  \frametitle{The Modification (Example 2)}
  Let's now use a different learning rate and see what happens.

  Recall: learning rate is the coefficient that we multiply the gradient by, in order to update the weight.

\end{frame}

\begin{frame}
  \frametitle{Demonstration (Example 2)}
  \animategraphics[width=\linewidth]{4}{figure/learning_rate/anim_}{00}{29}

\end{frame}

\begin{frame}
  \frametitle{The Pitfall}
  If the learning rate is too high, the model never converges and we can end up flung out of the optimal basin.
  
  If it's too low, the model takes too long to converge and can get stuck in the basins of local minima.

\end{frame}

\subsection{Neural Nets and Deep Learning}
\begin{frame}
  \frametitle{Conventions for Neural Nets}
  \begin{itemize}
    \item Same conventions as before

    \item $\sigma_{i} : \mathbb{R}^{p} \rightarrow \mathbb{R}^{p}$ will be generic nonlinear function associated with the $i$th hidden layer

    \item $H_{i} = \sbmqty{H_{11} & \cdots & H_{1q} \\ \vdots & \ddots & \vdots \\ H_{p1} & \cdots & H_{pq}} \in \mathbb{R}^{p \times q}$ will be the weights of the $i$th hidden layer

    \item $p$ and $q$ will be whatever the situation requires

    \item I will be ignoring biases, as the principles are the same for them as for weights

  \end{itemize}

\end{frame}

\subsubsection{Abstractly}
\begin{frame}
  \frametitle{Neural Nets}
  Neural nets are essentially ``chains'' of linear estimators, linked together with nonlinear functions.
  They're of the form
  \[
    \vu{y}
    =
    \sigma_{p} \pqty{H_{p} \sigma_{p - 1} \pqty{H_{p - 1} \cdots \sigma_{1} \pqty{H_{1} \va{x}}}}
  \]
  They have a lot more parameters to learn than linear estimators do, and so are quite computationally intensive.
  However, due to all of those degrees of freedom, the systems can learn complex behavior.

\end{frame}

\begin{frame}
  \frametitle{Stochastic Gradient Descent}
  Because of the large number of parameters, it is inefficient to compute the loss function and its gradient for \textit{every} $\va{x}$.
  A common solution to this issue is \textbf{stochastic gradient descent} (SGD).
  Instead of using every known $\va{x}$ to compute $L$, we use a small random batch of them.
  Since it's a random sample, it should be representative of all of the data we have.

  The downside of SGD is that the loss curve is less smooth, because the gradient for the sample will be different from the gradient for the whole population.

\end{frame}

\begin{frame}
  \frametitle{Overfitting}
  The increased complexity of neural networks can lead to \textbf{overfitting}.
  The network memorizes the particulars of the data set that you are training it on, instead of learning the general rules you want.

  One method for mitigating this effect is introducing a metric for the model's complexity.
  For example, if we add the number of non-zero weights to the loss function, the estimator will be ``incentivized'' to learn a simple model.

\end{frame}

\begin{frame}
  \frametitle{Data Management}
  The main way to handle overfitting is by cutting the data into three chunks:
  \begin{enumerate}
    \item Training set - The largest set ($\approx \SI{80}{\percent}$), which we use to train the estimator

    \item Evaluation set - A small set ($\approx \SI{10}{\percent}$), which we use to evaluate the model after it has been trained

    \item Test set - A small set ($\approx \SI{10}{\percent}$), which we use to evaluate our hyperparameter choice.

  \end{enumerate}

  During training, the model can never be allowed to see data from the evaluation or test sets.

\end{frame}

\begin{frame}
  \frametitle{A Typical Workflow}
  \begin{enumerate}
    \item Receive and format data for evaluation.

    \item Separate \textit{randomly} into training set $T$, evaluation set $E$, and test set $V$ (for \textit{v}alidation).


    \item Train the estimator for 2000 iterations on $T$.

    \item Evaluate the model on $E$:
      \begin{enumerate}
        \item If the model is sufficient, hold onto it.

        \item If the model is insufficient, forget it.

      \end{enumerate}

    \item Tweak hyperparameters, repeat steps 1-4 10 times.

    \item Run the models we held onto on $V$.
      Whichever model does best on $V$ is our final model.

  \end{enumerate}

\end{frame}

\subsubsection{Concretely}
\begin{frame}
  \frametitle{A Concrete Example}
  A concrete example will wait until I talk about the NEO network.

\end{frame}

\subsection{Datasets}
\begin{frame}
  \frametitle{Good Datasets for Machine Learning}
  A good dataset for machine learning has the following characteristics:
  \begin{itemize}
    \item Large enough to remove random fluctuations

    \item Clearly documented with the meanings of each column\footnote{You'd be surprised.}


    \item Representative of the data it will see in the future

  \end{itemize}

\end{frame}

\subsection{Hyperparameters}
\begin{frame}
  \frametitle{Hyperparameters}
  The following are some of the hyperparameters that one must tune to develop a good model:
  \begin{itemize}
    \item Learning rate (mitigated by certain optimization methods)

    \item Optimization method

    \item Loss function

    \item Architecture (for neural nets only)
      \begin{itemize}
        \item Number of hidden layers

        \item Shape of hidden layers

        \item Nonlinearities introduced

        \item Complexity penalties

      \end{itemize}

  \end{itemize}
  The challenge with machine learning is that all of these hyperparameters are extremely context-dependent.

\end{frame}

\section{NEOs}
\begin{frame}
  \frametitle{Toy Model}
  I pointed some machine learning tools at a dataset about NEOs to:
  \begin{itemize}
    \item Learn how best to interact with the code

    \item Provide a proof-of-concept

    \item Explore the datasets available

  \end{itemize}

\end{frame}

\subsection{The Dataset}
\begin{frame}
  \frametitle{The Telescope}
  I used data from the \href{https://neowise.ipac.caltech.edu/}{NEOWISE telescope}, accessible through \href{http://irsa.ipac.caltech.edu/Missions/wise.html}{Caltech's IRSA system}.
  \begin{itemize}
    \item December 2009 - WISE launched, full-sky survey in four IR bands (\SI{3.4}{\micro\meter}, \SI{4.6}{\micro\meter}, \SI{12}{\micro\meter}, \SI{22}{\micro\meter})

    \item September 2010 - Cryogenics depleted, lost \SI{12}{\micro\meter} and \SI{22}{\micro\meter} bands

    \item February 2011 - WISE put into hibernation

    \item December 2013 - WISE reactivated as NEOWISE with the goal of looking for NEOs

    \item June 2018 - NEOWISE completes ninth coverage of the sky

  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{The Contents}
  The data the at retrieved came in several formats:
  \begin{itemize}
  \item Image data
    \begin{itemize}
    \item Image data

    \item Metadata (calibration masks, time and duration of exposure, location of image)
      
    \end{itemize}
    
  \item Object data
    \begin{itemize}
    \item Object ID
      
    \item Location in image (using fractional pixel coordinates?)
      
    \item Size
      
    \item Reflectivity
      
    \item Etc.
      
    \end{itemize}
    
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Controversy}
  Recently (June 14, 2018), the data from NEOWISE has come under a lot of scrutiny.
  \href{https://www.nytimes.com/2018/06/14/science/asteroids-nasa-nathan-myhrvold.html}{\includegraphics[width=\textwidth]{figure/nyt}}
  A lot of the diameter estimates' uncertainties are thought to be too small.
  Thankfully, I didn't use that column.

\end{frame}

\begin{frame}
  \frametitle{My Use}
  I only used one feature: one band (\SI{3.4}{\micro\meter}) of the image data.
  Each image was 1016 pixels $\times$ 1016 pixels $\times$ 1 band.

  My label was the number of objects accounted for in each image.

  I did minimal preprocessing (used no image metadata).
  This is for several reasons:
  \begin{enumerate}
    \item Simple proof-of-concept, so the emphasis was on learning the technology

    \item Future telescopes' metadata is unknown, so wanted minimal dependencies

    \item Not allowed much computational overhead

  \end{enumerate}

\end{frame}

\subsection{The Architecture(s)}

\begin{frame}
  \frametitle{Convolution Layers}
  Because image data is so structured, data scientists conventionally use convolutional neural networks for images.
  These work by sliding small (e.g., 5 $\times$ 5) learnable filters along the image.
  These filters identify aspects of the image (e.g., edges, corners of objects).

  If we use 12 (4 $\times$ 4 $\times$ 3) filters with stride of 4, and our original image is (1016 $\times$ 1016 $\times$ 3), we end up with a hidden layer of shape (254 $\times$ 254 $\times$ 12).

\end{frame}

\begin{frame}
  \frametitle{Pooling Layers}
  Once we have our convolved data, we generally reduce its size with a max pooling layer.
  We take a volume of the convolved layer, and replace that volume with one entry, whose value is the maximum of the input volume.

  If we have our 254 $\times$ 254 $\times$ 12 layer from before, and run a 2 $\times$ 2 $\times$ 1 pool on it, we end up with a layer that is
  127 $\times$ 127 $\times$ 12.

  This reduces the computational difficulty of the training by a lot.

\end{frame}

\begin{frame}
  \frametitle{Dropout Layers}
  Dropout layers present a way to reduce the complexity of a model.
  Instead of penalizing non-zero parameters, a dropout layer randomly makes some of the weights in a dense layer zero for a training cycle.
  This means that the model can't rely too heavily on any one connection, making it more robust.

\end{frame}

\begin{frame}
  \frametitle{The Architecture}
  An architecture which has worked moderately well is the following, using a momentum optimizer:
  \vspace{11pt}

  \fbox{\includegraphics[width=0.9\textwidth]{figure/architecture}}

\end{frame}

\subsection{The Results}
\begin{frame}
  \frametitle{The Results}
  Initially, error on the test set was often around \SI{3000}{\percent} after 100 training iterations.
  After tweaking some hyperparameters (a little pre-processing, changing the optimizer from SGD to momentum, setting the learning rate to \num{1e-6}, etc.), mean absolute error is now \SI{16}{\percent} after 100 training iterations (\SI{14}{\percent} after 200).

\end{frame}

\subsection{The Next Steps}
\begin{frame}
  \frametitle{Next Steps}
  \begin{itemize}
    \item Continual improvement of hyperparameters and architecture
      \begin{itemize}
        \item Easy to automate production and automation of new models

      \end{itemize}

    \item Add more data/metadata to the model

    \item Do more preprocessing of the data
      \begin{itemize}
        \item Use the masks provided in the metadata
      \end{itemize}

  \end{itemize}

\end{frame}

\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
