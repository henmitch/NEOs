\documentclass{beamer}

\usepackage{graphicx}

\title{Machine Learning for NEO Detection}
\author{Henry Mitchell (GSFC Intern)}
\date{Summer 2018}

\usetheme{CambridgeUS}
\usecolortheme{orchid}

\definecolor{links}{HTML}{2A1B81}
\hypersetup{colorlinks,linkcolor=links,urlcolor=links}

\begin{document}
\begin{frame}
  \maketitle
\end{frame}

\begin{frame}
  \frametitle{Overview}
  The goal of this project was to explore use cases for machine learning in detecting near-earth objects (NEOs).
  These slides cover the \hyperlink{data}{data} used in this project, the \hyperlink{regressor}{regressor} developed, the \hyperlink{classifier}{classifier} developed, and the \hyperlink{conclusions}{conclusions} drawn.
\end{frame}

\begin{frame}
  \frametitle{Data}
  \hypertarget{data}{The data} for this project came from the \href{https://neowise.ipac.caltech.edu/}{NEOWISE telescope}, a full-sky survey in two IR bands.

  The dataset CalTech provides includes \href{http://irsa.ipac.caltech.edu/applications/wise/}{image data and metadata} (location, exposure time, zero-point, estimated background, etc.), as well as \href{https://irsa.ipac.caltech.edu/ibe/docs/wise/merge/merge_p1bm_frm/}{data about the objects} (NEOs and other small bodies) that were present in the image (physical location, location in the image, estimated size, estimated mass, etc.).
  They also provide APIs for the \href{http://irsa.ipac.caltech.edu/ibe/docs/wise/neowiser/p1bm_frm/}{data} and \href{http://irsa.ipac.caltech.edu/ibe/docs/wise/neowiser/p1bm_frm/}{metadata}.

  The data this project used were the image data (and location metadata), the objects' locations in the images and the sky, and the number of objects in each image.

\end{frame}

\begin{frame}
  \frametitle{Regressor}
  The \hypertarget{regressor}{first} application attempted was a regressor to use image data to guess the number of objects in a given image.
  Using images (with no pre-processing) as features and a count of the entries in the object data lists for those images as labels, an optimal set of hyperparameters was found.
  The architecture was a modification of a \href{https://www.tensorflow.org/tutorials/estimators/cnn}{TensorFlow tutorial} for using their Estimators API to create a CNN.

  The optimal model's architecture was as follows:
  20-filter convolutional, max pooling, 10-filter convolutional, max pooling, flattening, dense, dense, dropout.

  \textit{It was found to be easy to programatically test models with various architectures and hyperparameters, which is a useful technique for finding optimal models.}

\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{figure/arch}
    \caption{The optimal architecture.
 The black boxes are the outputs of the various layers (except the first, which is the input image).
 Their height corresponds to the height/width of the image, and are all to scale except the dotted first flattened layer.
  Their width corresponds to the number of filters that have been applied.
  Red lines represent convolutional layers, blue are pooling, gray is flattening, pink is dense, and cyan is dropout.}
    \label{fig:arch}
  \end{figure}%
  Both convolutional layers used 3-by-3 kernels, and the optimizer was an AdaGrad with an initial learning rate of $10^{-6}$.
  This architecture achieved a relative error of 1.2\%.
\end{frame}

\begin{frame}
  \frametitle{Classifier}
  The \hypertarget{classifier}{second} application attempted was an image segmenter which would classify a given pixel as either an object or not.
  The features for this model were the image data subtracted from a mosaic of other images in the same location (created using \href{http://montage.ipac.caltech.edu/docs/index.html}{Montage}).
  The labels were arrays, generated from the object datasets, with zeros for pixels which were not in objects, ones for pixels which were in objects (visible \hyperlink{figs}{here}).

  This application was attempted using resolutional neural nets (ResNets), fully convolutional neural nets (FCNs), and deep convolutional neural nets (deep CNNs).
  Accuracy, $F_{1}$ score, Matthews Correlation Coefficient (MCC), and cross-entropy were used as loss functions in order to reduce the effect of class imbalance.
  Additionally, the pixels in objects were given a $10 \times 10$ border, also to reduce class imbalance.

  Despite the variety of techniques attempted, this application was not successful.

\end{frame}

\begin{frame}
  \frametitle{ResNet}
  \begin{minipage}{0.45\textwidth}
    One attempted architecture was a \href{https://arxiv.org/pdf/1512.03385.pdf}{ResNet}.
    ResNets bring outputs from earlier layers to later layers, in order not to lose the influence of earlier layers.

    They are a standard method for image segmentation.
  \end{minipage}\hfill%
  \begin{minipage}{0.45\textwidth}
    \fbox{\includegraphics[height=0.8\textheight,keepaspectratio]{figure/resnet}}
  \end{minipage}

\end{frame}

\begin{frame}
  \frametitle{FCN}
  \begin{minipage}{0.45\textwidth}
    Another attempted architecture was an \href{https://people.eecs.berkeley.edu/~jonlong/long_shelhamer_fcn.pdf}{FCN}.
    As the name suggests, they are neural networks that have no dense layers.
    They are comprised entirely of convolutional and pooling layers.

    They are also an industry standard for image segmentation, and were the driving force behind an algorithm developed to solve a similar problem (\href{https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=\&arnumber=7533106\&tag=1}{identifying birds on empty sky near wind turbines}).

  \end{minipage}\hfill%
  \begin{minipage}{0.45\textwidth}
    \fbox{\includegraphics[height=0.8\textheight,keepaspectratio]{figure/fcn}}
  \end{minipage}

\end{frame}

\begin{frame}
  \frametitle{Results}
  \hypertarget{figs}{The} model that trained for longest ended up getting results that looked similar to what follows:
  \vspace{11pt}

  \begin{minipage}{0.3\linewidth}
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{figure/input}
      \caption{The feature}
      \label{fig:input}
    \end{figure}
  \end{minipage}\hfill%
  \begin{minipage}{0.3\linewidth}
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{figure/expect}
      \caption{The label}
      \label{fig:label}
    \end{figure}
  \end{minipage}\hfill%
  \begin{minipage}{0.3\linewidth}
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{figure/output}
      \caption{The output}
      \label{fig:output}
    \end{figure}
  \end{minipage}

  The model learned the black border around the image, which was simply an artifact of the method of generating the labels.
  No models got above 16\% precision, or 62\% recall.


\end{frame}

\begin{frame}
  \frametitle{Conclusions and Future Pursuits}
  \hypertarget{conclusions}{For} future investigation of these techniques, some considerations should be taken:
  \begin{itemize}
  \item GPUs allow for more rapid development, testing, and deployment of machine learning models.
    If machine learning is to seriously be pursued, access to GPUs is necessary.

  \item Machine learning can not do better than the current techniques, as they provide the labels the models use as ``ground truth''.

  \item For any machine learning applications, it is helpful to have subject-matter experts in data science and in the field of the application.

  \item Programmatically testing various architectures is relatively easy.

  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Conclusions and Future Pursuits}
  \begin{itemize}
  \item Machine learning is best suited for two types of problems:
    \begin{enumerate}
    \item Finding relationships that are obvious to humans, but hard to algorithmically describe (i.e., image recognition)

    \item Finding relationships that are non-obvious to humans, but easy to algorithmically describe (i.e., clustering)

    \end{enumerate}

    NEO detection falls between these two categories, as it is neither obvious to humans (without a lot of finessing) or easy to algorithmically describe.

    As was best put by Dave Cottingham, machine learning tools are generally best for solving problems that ``aren't subtle.''

  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{More Information}
  A fuller description of the specifics of this project is \href{https://henmitch.gitlab.io/NEOs/index.html}{available online}.
  It includes \href{https://henmitch.gitlab.io/NEOs/ml_resources/data_resources.html}{descriptions of the datasets}, \href{https://henmitch.gitlab.io/NEOs/usage/cnn.html}{descriptions of the applications}, as well as \href{https://henmitch.gitlab.io/NEOs/about/acknowledgements.html\#useful-resources}{a list of some useful resources}.

\end{frame}

\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

%% Last updated: Fri Aug 10 09:00:41 2018